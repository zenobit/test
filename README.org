* Pages
This is the codeberg repo for my [[https://zortazert.codeberg.page][website]]. Codeberg has a feature to host webwsites. It is pretty awesome that I can do website stuff by adding files to this repo rather then having a server and all that complicated stuff.

* What's here?
    - *Tutorials*. I for some strange reason like to make tutorials a lot.
    - *Animations*. Sometimes I will create animations in blender. So rather then just posting them on a video platform why not post it also on this website.

* How is this happening?
    - Blogs are done with org export. I really like org mode. Org can be exported to multiple formats like html so I just use that.
    - For the main page I use html. It has all the website links and info.
    - For RSS I generate it with a python script.
