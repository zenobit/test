#+TITLE: Programming Languages I use.
#+HTML_HEAD: <link rel='stylesheet' type='text/css' href='styles.css' />
#+OPTIONS: html-style:nil toc:nil num:nil 

* Python
  For basicly all automation, creating gui apps and much more. The sky is the limit with python. I love python.
* Markdown languages
** MD
   I use MD rarely because I don't like the amount of versions it has and the syntax if italics and bold I really don't like. It simply isn't the best. I do like using it sometimes though
** ORG
   Org is a special language. You can do so much with it. It's readable, it's beatifule and makes my life better.
* Web dev
** HTML
   HTML I like using for some of my website pages. Though for blogs it's easier to use ORG to HTML.
** CSS
   I really want a really nice website and CSS is supposed to make that acheivable. But it kinda isn't working for me. I can live with it though.
* Shell Script
  I haven't used it in a while. You can do most everything in it except for GUI applications etc. I don't like it for the main reason of it doesn't work on all platforms. It is limited to whatever has a shell and some software.
* Greasemonkey
  I don't know this language that well. I have just made a repo with community scripts in it for redirecting bad none privacy respecting links to better ones. It's very cool.

#+BEGIN_EXPORT html 
<hr> 
<footer> 
<a rel='license' href='http://creativecommons.org/licenses/by-sa/4.0/'><img alt='Creative Commons License' style='border-width:0' width='88' height='31' src='../images/cc-by-sa.png' /></a><br> 
Unless otherwise noted, all content on this website is Copyright Zortazert 2021 and is licensed under <a rel='license' href='http://creativecommons.org/licenses/by-sa/4.0/'>CC BY-SA 4.0</a>. 
</footer> 
#+END_EXPORT 
